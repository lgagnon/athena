/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_TRACKPAMEASUREMENTS_H
#define XAODTRACKING_TRACKPAMEASUREMENTS_H

#include "versions/TrackMeasurements_v1.h"

namespace xAOD {
  typedef TrackMeasurements_v1 TrackMeasurements;
}
#endif